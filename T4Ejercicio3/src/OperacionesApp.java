
public class OperacionesApp {

	public static void main(String[] args) {
		int X=5, Y=6;
		double N=3.56, M=5.65;
		int suma = X + Y;
		int diferencia = X - Y;
		int producto = X * Y;
		int cociente = X / Y;
		int resto = X % Y;
		double sumaD = N + M;
		double diferenciaD = N - M;
		double productoD = N * M;
		double cocienteD = N / M;
		double restoD = N % M;
		double sumaC = X + N;
		double cocienteC = Y / M;
		double restoC = Y % M;
		double dobleX = X * 2;
		double dobleY = Y * 2;
		double dobleN = N * 2;
		double dobleM = M * 2;
		double sumaTotal = X + Y + N + M;
		double productoTotal = X * Y * N * M;
		
		System.out.println("El valor de cada variable es: X=" + X + " Y=" + Y + " N=" + N + " M=" + M);
		System.out.println("La suma de X + Y es: " + suma);
		System.out.println("La diferencia de X - Y es: " + diferencia);
		System.out.println("La producto de X * Y es: " + producto);
		System.out.println("La cociente de X / Y es: " + cociente);
		System.out.println("La resto de X / Y es: " + resto);
		
		System.out.println("La suma de N + M es: " + sumaD);
		System.out.println("La diferencia de N - M es: " + diferenciaD);
		System.out.println("La producto de N * M es: " + productoD);
		System.out.println("La cociente de N / M es: " + cocienteD);
		System.out.println("La resto de N % M es: " + restoD);
		
		System.out.println("La suma de X + N es: " + sumaC);
		System.out.println("La cociente de Y / M es: " + cocienteC);
		System.out.println("La resto de Y % M es: " + restoC);
		
		System.out.println("El doble de X=" + dobleX + " Y=" + dobleY + " N=" + dobleN + " M=" + dobleM);
		
		System.out.println("La suma de todas las variables es: " + sumaTotal);
		System.out.println("El producto total es: " + productoTotal);
		
		
		
		
		
		
		
		
		
		

	}

}
